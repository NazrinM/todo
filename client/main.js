import {Template} from 'meteor/templating';
import {ReactiveVar} from 'meteor/reactive-var';
import {Random} from 'meteor/random'

import './main.html';

Template.hello.onCreated(function helloOnCreated() {
    // counter starts at 0
    this.counter = new ReactiveVar(0);
    this.todos = new ReactiveVar([])
});

Template.hello.helpers({
    counter() {
        return Template.instance().counter.get();
    },
    todos() {
        return Template.instance().todos.get()
    }
});

Template.hello.events({
    // 'click button'(event, instance) {
    //     // increment the counter when button is clicked
    //     instance.counter.set(instance.counter.get() + 1);
    // },
    'submit #newTodo'(e, temp) {
        e.preventDefault();
        let target = e.target;
        let todo = target.todo.value;
        let data = {
            _id: Random.id(),
            todo,
            checked: false
        }
        let todos = temp.todos.get();
        todos = [...todos, data]
        temp.todos.set(todos)
        console.log(temp.todos.get())
    },
    'change .checkTodo'(e, temp) {
        console.log(this)
        let _id = this._id
        let todos = temp.todos.get();
        let index=todos.findIndex((e)=>(e._id===_id))
        todos[index].checked=!todos[index].checked;
        temp.todos.set(todos)
        console.log(todos)
    }

});
